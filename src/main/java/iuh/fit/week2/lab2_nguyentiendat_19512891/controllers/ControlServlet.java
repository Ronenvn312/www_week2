package iuh.fit.week2.lab2_nguyentiendat_19512891.controllers;

import iuh.fit.week2.lab2_nguyentiendat_19512891.common.enumeration.EmployeeStatus;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Customer;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Employee;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Order;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.CustomerService;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.EmployeeService;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.OrderService;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.ProductImageService;
import jakarta.inject.Inject;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebServlet(urlPatterns = {"/ControlServlet"})
public class ControlServlet extends HttpServlet {
    @Inject
    private EmployeeService service;
    @Inject
    private CustomerService customerService;
    @Inject
    private OrderService orderService;
    @Inject
    private ProductImageService productImageService;

    public boolean createOrder() {
        Employee e = new Employee("emp1", "khai baby", LocalDate.of(2002, 5, 29), "khai@mail", "032423", "tan binh", EmployeeStatus.ACTIVE);
        service.create(e);
        Customer c = new Customer("cust1", "khai not baby", "khai@mail", "0234143", "tan binh");
        customerService.create(c);
        Order o = new Order("order1", LocalDateTime.now(), e, c);
        orderService.create(o);
        return true;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        List<ProductImage> l = productImageService.findAll();
//        req.setAttribute("products",l);
        RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }


}
