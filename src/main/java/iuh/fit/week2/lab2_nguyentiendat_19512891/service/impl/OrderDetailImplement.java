package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.OrderDetail;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository.OrderDetailRepository;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.OrderDetailService;
import jakarta.inject.Inject;

public class OrderDetailImplement implements OrderDetailService {
    @Inject
    private OrderDetailRepository repository;
    @Override
    public boolean create(OrderDetail orderDetail) {
        return repository.create(orderDetail);
    }
}
