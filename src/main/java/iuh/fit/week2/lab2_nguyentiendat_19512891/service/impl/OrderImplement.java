package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Order;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.OrderService;
import jakarta.inject.Inject;

import java.time.LocalDate;
import java.util.List;

public class OrderImplement implements OrderService {
    @Inject
    private OrderService repository;
    @Override
    public boolean create(Order o) {
        return repository.create(o);
    }

    @Override
    public List<Order> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Order> findOrdersByDate(LocalDate date) {
        return repository.findOrdersByDate(date);
    }

    @Override
    public List<Order> findOrdersByDateRange(LocalDate s, LocalDate e) {
        return repository.findOrdersByDateRange(s,e);
    }

    @Override
    public List<Order> findOrdersByEmployeeDateAndRange(LocalDate s, LocalDate e, String em) {
        return repository.findOrdersByEmployeeDateAndRange(s,e,em);
    }
}
