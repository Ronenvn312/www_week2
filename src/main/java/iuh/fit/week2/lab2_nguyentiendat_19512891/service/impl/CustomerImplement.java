package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Customer;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository.CustomerRepository;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.CustomerService;
import jakarta.inject.Inject;

import java.util.List;

public class CustomerImplement implements CustomerService {
    @Inject
    private CustomerRepository repository;

    @Override
    public boolean create(Customer customer) {
        return repository.createCust(customer);
    }

    @Override
    public List<Customer> findAll() {
        return repository.findAll();
    }
}
