package iuh.fit.week2.lab2_nguyentiendat_19512891.service;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Employee;

import java.util.List;

public interface EmployeeService {
    boolean create(Employee e);
    List<Employee> findAll();
}
