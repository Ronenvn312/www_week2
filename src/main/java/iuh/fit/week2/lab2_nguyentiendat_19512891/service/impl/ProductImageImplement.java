package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.ProductImage;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository.ProductImageRepository;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.ProductImageService;
import jakarta.inject.Inject;

import java.util.List;

public class ProductImageImplement implements ProductImageService {
    @Inject
    private ProductImageRepository repository;
    @Override
    public List<ProductImage> findAll() {
        return repository.findAll();
    }
}
