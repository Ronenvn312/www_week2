package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Employee;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository.EmployeeRepository;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.EmployeeService;
import jakarta.inject.Inject;

import java.util.List;

public class EmployeeImplement implements EmployeeService {
    @Inject
    private EmployeeRepository repository;
    @Override
    public boolean create(Employee e) {
        return repository.create(e);
    }

    @Override
    public List<Employee> findAll() {
        return repository.findAll();
    }
}
