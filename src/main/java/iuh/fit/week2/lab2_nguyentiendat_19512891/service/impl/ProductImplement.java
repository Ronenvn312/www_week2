package iuh.fit.week2.lab2_nguyentiendat_19512891.service.impl;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Product;
import iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository.ProductRepository;
import iuh.fit.week2.lab2_nguyentiendat_19512891.service.ProductService;
import jakarta.inject.Inject;

import java.util.List;

public class ProductImplement implements ProductService {
    @Inject
    private ProductRepository repository;
    @Override
    public boolean create(Product product) {
        return repository.create(product);
    }

    @Override
    public List<Product> findAll() {
        return repository.findAll();
    }
}
