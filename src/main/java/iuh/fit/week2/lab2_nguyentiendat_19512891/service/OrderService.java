package iuh.fit.week2.lab2_nguyentiendat_19512891.service;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Order;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {
    boolean create(Order o);
    List<Order> findAll();
    List<Order> findOrdersByDate(LocalDate date);
    List<Order> findOrdersByDateRange(LocalDate s, LocalDate e);
    List<Order> findOrdersByEmployeeDateAndRange(LocalDate s, LocalDate e, String em);
}
