package iuh.fit.week2.lab2_nguyentiendat_19512891.service;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.OrderDetail;

public interface OrderDetailService {
    boolean create(OrderDetail orderDetail);
}
