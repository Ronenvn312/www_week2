package iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jdk.jfr.Percentage;

public class Connection {
    private static Connection instance;
    private EntityManagerFactory emf;

    private Connection() {
        emf= Persistence.createEntityManagerFactory("jpa-hibernate-mysql");

    }

    public static Connection getInstance() {
        if(instance==null)
            instance= new Connection();
        return instance;
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }
}
