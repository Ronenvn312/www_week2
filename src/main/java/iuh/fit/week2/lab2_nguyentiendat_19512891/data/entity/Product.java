package iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity;

import iuh.fit.week2.lab2_nguyentiendat_19512891.common.enumeration.ProductStatus;
import jakarta.persistence.*;

@Entity
@Table(name = "product")
@NamedQueries(
        @NamedQuery(name = "Product.findAll",query = "SELECT e FROM Product e WHERE e.status = 1")
)
public class Product {
    @Id
    @Column(name = "product_id")
    private String productId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "unit")
    private String unit;
    @Column(name = "manufacturer_name")
    private String manufacturerName;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private ProductStatus status;

    public Product(String productId, String name, String description, String unit, String manufacturerName, ProductStatus status) {
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.unit = unit;
        this.manufacturerName = manufacturerName;
        this.status = status;
    }

    public Product() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }
}
