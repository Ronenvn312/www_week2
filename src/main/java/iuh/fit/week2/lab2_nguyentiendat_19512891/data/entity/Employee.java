package iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity;


import iuh.fit.week2.lab2_nguyentiendat_19512891.common.enumeration.EmployeeStatus;
import jakarta.inject.Named;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "employee")
@NamedQueries(
        @NamedQuery(name = "Employee.findAll",query = "SELECT e FROM Employee e WHERE e.status = 1")
)
public class Employee {
    @Id
    @Column(name = "emp_id")
    private String empId;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "dob")
    private LocalDate dob;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;
    @Column(name = "address")
    private String address;
    @Enumerated(EnumType.ORDINAL)
    private EmployeeStatus status;

    public Employee(String empId, String fullName, LocalDate dob, String email, String phone, String address, EmployeeStatus status) {
        this.empId = empId;
        this.fullName = fullName;
        this.dob = dob;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.status = status;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }

    public Employee() {
    }
}
