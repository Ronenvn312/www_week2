package iuh.fit.week2.lab2_nguyentiendat_19512891.data.repository;

import iuh.fit.week2.lab2_nguyentiendat_19512891.data.entity.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;

import java.util.List;

public class EmployeeRepository {
    private EntityManager em;
    private EntityTransaction tr;

    public EmployeeRepository() {
        em = Connection.getInstance().getEmf().createEntityManager();
        tr = em.getTransaction();
    }

    public boolean create(Employee e) {
        tr.begin();
        try {
            em.persist(e);
            tr.commit();
            return true;
        } catch (Exception ex) {
            tr.rollback();
            ex.printStackTrace();
        }
        return false;
    }

    public boolean update(Employee e) {
        tr.begin();
        try {
            em.merge(e);
            tr.commit();
            return true;
        } catch (Exception ex) {
            tr.rollback();

        }
        return false;
    }

    public List<Employee> findAll() {
        List<Employee> l = em.createNamedQuery("Employee.findAll", Employee.class).getResultList();
        return l;
    }
}
